# Sesión 5: Lógica y  pensamiento computacional

## Lógica y algoritmo (programa)

Te invitamos a ver el siguiente vídeo sobre el pensamiento computacional, te servirá
para realizar las actividades de esta semana.

<video style="max-width: 100%; height: auto;" controls >
    <source src="./video1-pensamiento-computacional.mp4" type="video/mp4">
</video> 

Vídeo tomado del siguiente [link de youtube](http://youtube.com/watch?v=ti315UlVtS4)

> **Atención**: Sino puedes reproducir el vídeo, búscalo en la carpeta de las **sesión 5** como:
>
> `video1-pensamiento-computacional.mp4`

Te invitamos a ver el siguiente vídeo sobre el pensamiento computacional, te servirá
para realizar las actividades de esta semana.

<video style="max-width: 100%; height: auto;" controls >
    <source src="./video2-algoritmo.mp4" type="video/mp4">
</video> 

Vídeo tomado del siguiente [link de youtube](http://youtube.com/watch?v=U3CGMyjzlvM)

> **Atención**: Sino puedes reproducir el vídeo, búscalo en la carpeta de las **sesión 5** como:
>
> `video2-algoritmo.mp4`

## Actividad 1: Diez acertijos para resolver con lógica

Resuelve y anota la solución en tu cuaderno de los acertijos que te presento a continuación; no te preocupes sino
resuelves alguno, pero sí inténtalo.

> Acertijo 1 - El laberinto Estás escapando de un laberinto, y hay tres puertas frente a ti. La puerta de la izquierda conduce a un infierno. La puerta en el centro te lleva a un asesino mortal. La puerta de la derecha te conduce a un león que no ha comido en tres meses. ¿Qué puerta eliges?

> Acertijo 2 - Viajando en familia Dos personas viajan en coche. La menor es hija de la mayor, pero la mayor no es su padre. ¿Quién es? 

> Acertijo 3- Monedas El padre de Juan le dice a su hijo que le va a otorgar dos monedas de curso legal. “Entre las dos suman tres euros, pero una de ellas no es de un euro”. ¿Cuáles son las monedas?
placeholder 
> Acertijo 4 - Lado y corte Ponedme de lado y seré todo. Cortadme a la mitad y seré nada. ¿Qué soy?

> Acertijo 5 - Caída ¿Cómo puede sobrevivir alguien que cae de un edificio de 50 pisos?

> Acertijo 6 - Reloj de arena Si tienes un reloj de arena de 7 minutos y otro igual de 11 minutos, ¿cómo puedes hervir un huevo en exactamente 15 minutos?

> Acertijo 7 - La bifurcación Estás paseando por un camino y de pronto entras en una bifurcación. Un camino lleva a una muerte segura; el otro conduce a la felicidad eterna. No sabes cuál es cuál. En el medio del comienzo de la bifurcación, te encuentras con dos hermanos que saben qué camino es el bueno. Un hermano siempre dice la verdad y el otro siempre miente. Sólo puedes hacerles una pregunta. ¿Cómo determinarías qué camino tomar, o sea, el de la felicidad eterna?

> Acertijo 8 - Continúa con la secuencia ¿Qué sigue en esta secuencia de números: 1, 11, 21, 1211, 111221, 312211, ______?

> Acertijo 9 - Caballos Un granjero tiene 10 conejos, 20 caballos y 40 cerdos. Si llamamos “caballos” a los “cerdos”, ¿cuántos caballos tendrá?

> Acertijo 10 - Bolsa de oro Hay cinco bolsas de oro que parecen idénticas, y cada una tiene diez piezas de oro. Una de las cinco bolsas tiene oro falso. El oro verdadero, el oro falso y las cinco bolsas son idénticos en todos los sentidos, excepto que las piezas de oro falso pesan 1.1 gramos cada una, y las piezas de oro reales pesan 1 gramo cada una. Tienes una pesa digital superprecisa y puedes usarla solo una vez. ¿Cómo determinarás qué bolsa tiene el oro falso?

## Actividad 2: Creando instrucciones para resolver un problema

![huevos](./la_bola_más_pesada.png)

Resuelve este enigma: tenemos un juego de 9 huevos de la misma apariencia, pero una de ellos pesa más que el resto.
Para poder identificarlo tenemos una balanza tradicional, pero sólo se podía utilizar dos veces.
¿Cómo es posible saber cuál es? La lógica y el ingenio nos darán la solución

Describa el algoritmo (paso a paso) para solucionar dicho juego.

> Ejemplo de descripción  de una situación en un algoritmo (paso a paso):
> - Algoritmo para llenar un vaso de agua.

> Solución por pasos:
>
> INICIO
>
> 1. Tomar el vaso
> 2. Abrir el grifo
> 3. Colocar el vaso bajo el grifo
> 4. Esperar que el vaso se llene
> 5. Retirar el vaso
> 6. Cerrar el grifo
>
> FIN

## Actividad 3: Crear algoritmo de el videojuego en Scratch

Esta actividad consiste en realizar lo más detallado posible el algoritmo (paso a paso) para describir las situaciones
del juego que estamos creando, si has revisado las actividades opcionales sobre "Mi primer juego en Scratch" podrás
identificar que los bloques permiten escribir el algoritmo (paso a paso).

Describe tu paso a paso de manera similar a lo que hemos hecho con el enigma de los huevos

## Actividad 4 (Opcional): Implementación de vídeo juego en scratch

Ve a los siguientes enlaces y realiza las tareas allí establecidas, empezarás a programar
tu primer vídeo juego:
- [Gravedad y muerte de personaje](../../juego-scratch/5.-Agregar-gravedad-y-muerte-al-personaje/tutorial5.md)
- [Meta y cambio de nivel](../../juego-scratch/6.-Meta-y-cambio-de-nivel/tutorial6.md)
- [Agregar enemigos y daño](../../juego-scratch/7.-Agregar-enemigos-y-dano/tutorial7.md)
