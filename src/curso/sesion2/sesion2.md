# Sesión 2: La revolución industrial y sus implicaciones 

Hola, te damos la bienvenida a nuestra primera semana, para que sepas
de qué se trata, dale play al siguiente audio:

<audio controls>
    <source src="./audio1-indicacion-semana.mp3" type="audio/mpeg">
    Your browser does not support the audio element.
</audio>

## Actividad 1: ¿Cómo es vivir en la Cuarta Revolución Industrial?

Observa el siguiente vídeo y responde las siguientes preguntas:

* ¿Qué es la cuarta revolución industrial?
* ¿Que se requiere para aprovechar las oportunidades de la cuarta revolución
industrial?
* ¿Qué papel puede desempeñar la educación en esa nueva era?
* ¿Qué habilidades crees que son necesarias desarrollar para aprovechar las
oportunidades?
* ¿Cómo se puede hacer para que las personas no pierdan sus empleos en este
contexto de la cuarta revolución industrial?

<video style="max-width: 100%; height: auto;" controls >
    <source src="./video1-cuarta-revolucion-industrial.mp4" type="video/mp4">
</video> 

Vídeo tomado del siguiente [link de youtube](https://youtu.be/-OiaE6l8ysg)

> **Atención**: Sino puedes reproducir el vídeo, búscalo en la carpeta de las **sesión 2** como:
>
> `video1-cuarta-revolucion-industrial.mp4`

## Actividad 2: Las implicaciones negativas del uso de la Tecnología

En el siguiente vídeo observarás una realidad dada por la falta de una economía circular
que considere de manera responsable qué hacer con nuestro desechos, verás como existen
poblaciones que desarrollan mercados a partir de esos desechos; anota en tu cuaderno
las ideas relevantes que queden en tu mente que te generen impacto, construye en tu
cuaderno un **collage** de imágenes de revista o de periódico que representen la situación
documentada en el vídeo.

<video style="max-width: 100%; height: auto;" controls >
    <source src="./video2-vertedero-de-basura-mundial.mp4" type="video/mp4">
</video> 

Video tomado del siguiente [link de youtube](https://youtu.be/lGdgtUDE9TY)

> **Atención**: Sino puedes reproducir el vídeo, búscalo en la carpeta de las **sesión 2** como:
>
> `video2-vertedero-de-basura-mundial.mp4`

## Actividad 3: La comida de hoy es menos nutritiva

Mira el siguiente vídeo e investiga con tus compañeros
cuales pueden ser las razones por las cuales existe un
aumento en el dióxido de carbono **CO2**, analiza el impacto que tiene
esto en la capacidad nutritiva delos alimentos que nos dan las plantas.
Además investiga con tus compañeros que tiene que ver las semillas
transgénicas con la selección artificial que se expone en el vídeo.
Documenta tus reflexiones en tu cuaderno.

<video style="max-width: 100%; height: auto;" controls >
    <source src="./video3-la-comida-de-hoy.mp4" type="video/mp4">
</video> 

Video tomado del siguiente [link de youtube](https://youtu.be/vyM9GuEE9co)

> **Atención**: Sino puedes reproducir el vídeo, búscalo en la carpeta de las **sesión 2** como:
>
> `video3-la-comida-de-hoy.mp4`

## Actividad 4: Situación o problemática en mi contexto

Viendo los aspectos positivos y negativos que trae la tecnología, te invito a compartir
con tus familiares, compañeros y amigos sobre una situación que les gustaría cambiar
o transformar de su realidad (de su diario vivir), teniendo en cuenta las emociones,
motivaciones, preocupaciones, sino sabes por donde empezar, te invito a ver las siguientes
preguntas que te servirán de guía:

* ¿A quién quieres ayudar o quién afecta?
* ¿A quiénes necesitaría para hacerlo?
* ¿Qué elementos necesitarías?
* ¿Que herramientas construirías?

Comparte tus reflexiones en tu cuaderno.

## Actividad 5: Generar la narrativa del juego

La semana pasada hablamos sobre el contexto y el argumento del juego ahora queremos
que te pienses una historia que suceda en ese lugar, deberás crear un inicio, un fin
o propósito, puedes apoyarte creando un cuento, que tenga un personaje, una historia
y que esa historia tenga que ver con las situaciones trabajadas en las actividades
de esta semana, es decir, sobre la tecnología como una herramienta que permite 
optimizar actividades o tareas, pero que también tiene sus cosas no tan positivas.

Para más explicación de la actividad escucha el siguiente audio

<audio controls>
    <source src="./audio3-instrucciones-juego.mp3" type="audio/mpeg">
    Your browser does not support the audio element.
</audio>

> **Atención**: Sino puedes reproducir el audio, búscalo en la carpeta de las sesión 2 como:
>
> `audio3-instrucciones-juego.mp3`

## Cierre de sesión 2
