# Sesión 1: La tecnología como un fenómeno

Hola, te damos la bienvenida a nuestra primera semana, para que sepas
de qué se trata, dale play al siguiente audio:

<audio controls>
    <source src="./audio1-indicacion-semana.mp3" type="audio/mpeg">
    Your browser does not support the audio element.
</audio>

> **Atención**: Sino puedes reproducir el audio, búscalo en la carpeta de la **sesión 1** como:
>
> `audio1-indicacion-semana.mp3`

## Actividad 1: ¿Qué es ser campesino?

Mira el siguiente vídeo relacionado al campo y al significado de ser campesino,
después de ver el vídeo, toma tu cuaderno, pon el título de la actividad y
descríbenos en dos párrafos lo que significa para ti:

* ¿Qué es ser campesino?
* ¿Qué significa el campo para ti?
* ¿Qué es lo que más amas del campo?
* ¿Qué es lo que consideras más importante para ti del campo?
* ¿Qué crees que se necesita para que las personas quieran quedarse en los campos y no irse a las ciudaddes?

**Observación**: Puedes hacer dibujos o diagramas que acompañen lo que has escrito, inclusive, puedes
usar otros medios para expresar lo que te hemos pedido, a través de hojas de árboles, cáscaras de frutas,
barro, lo que te imagines.

<video style="max-width: 100%; height: auto;" controls>
    <source src="./video1-ser-campesino.mp4" type="video/mp4">
</video> 

Vídeo tomado del canal [link de youtube](https://youtu.be/ZROUG_Ge1y0)

> **Atención**: Sino puedes reproducir el vídeo, búscalo en la carpeta de las **sesión 1** como:
>
> `video1-ser-campesino.mp4`

## Actividad 2: ¿Que es la tecnología?

Mira el siguiente vídeo relacionado a la tecnología y responde lo siguiente:

1. ¿Para ti qué es la tecnología?
2. ¿Crees un azadón, un machete, una orqueta o una cauchera son tecnologías?
¿Explica porqué lo son o porqué no son tecnología? ¿Da ejemplos de tecnología?
5. En sí ¿Para qué sirve la tecnología?
4. ¿Que opinas de lo siguiente?: La tecnología nos deshumaniza
5. ¿Con ayuda de la tecnología podríamos aprender algo nuevo? Justifica tu respuesta

<video style="max-width: 100%; height: auto;" controls>
    <source src="./video2-que-es-la-tecnologia.mp4" type="video/mp4">
</video> 

Vídeo tomado del canal [link de youtube](https://youtu.be/oL_mHK5v2OU)

> **Atención**: Sino puedes reproducir el vídeo, búscalo en la carpeta de las **sesión 1** como:
>
> `video2-que-es-la-tecnologia.mp4`

## Actividad 3: ¿Cuál es la relación de la tecnología con el campo?

Mira el siguiente vídeo y piensa en las posibilidades tecnológicas en el campo,
responde en tu cuaderno la siguiente pregunta:

* ¿Qué se requiere hacer para que las personas encuentren oportunidades
de crecimiento personal y profesional en el campo?

<video style="max-width: 100%; height: auto;" controls >
    <source src="./video3-la-tecnologia-en-la-agricultura.mp4" type="video/mp4">
</video> 

Vídeo tomado del canal [link de youtube](https://youtu.be/5R3R7zLbJcw)

> **Atención**: Sino puedes reproducir el vídeo, búscalo en la carpeta de las sesión 1 como:
>
> `video3-la-tecnologia-en-la-agricultura.mp4`

Información que permita poner una perspectiva de la tecnología (agroindustrial) podcast o un vídeo
o varios podcast y una **lectura**

## Actividad 4: Exploremos y reflexionemos

Te quiero invitar a que hagas una actividad de observación; haz una caminata desde
casa hasta un lugar que prefieras, por ejemplo, tu colegio, o lugares donde vayan
varias personas, mientras que caminas observa los detalles del paisaje, de los lugares
que atraviesas, de las actividades que realizan las personas, toma fotografías y haz
dibujos con los detalles que consideres relevantes. Cuando ya hayas realizado la actividad
responde lo siguiente en tu cuaderno:

1. Según su entorno ¿Qué rasgos tecnológicos identifican? (Al mirar a su alrededor, piensen como la tecnología a transformado o cambiado su entorno) (por ejemplo: el uso del azadón, tractor, etc)
2. ¿Qué representa la tecnología para usted?
3. Teniendo en cuenta que un fenómeno es algo que se manifiesta, que puede transformar o cambiar y se puede percibir a través de los sentidos
¿Porque la tecnología se podría considerar como un fenómeno?
4. ¿Cúal es su relación con la tecnología? (Como es tu día a día de la mano con la tecnología).
5. Piensa y responde como crees ¿Que la tecnología nos transforma o como nosotros transformamos las tecnología?

## Actividad 5: El contexto de un vídeo juego 

![super-mario](./super-mario.gif)

Quiero invitarte a que construyamos juntos recursos para la creación de un vídeo juego,
esta semana hablaremos sobre el contexto: el lugar donde queremos que se desarrolle un juego
y parte del argumento del juego; el argumento es el alma del juego, el porqué del juego.

Para tal fin te invito a que revises las actividades que ya has realizado y mientras lo haces,
imagina las posibilidades tecnológicas que pueden haber en tu comunidad y vereda, imagina
que construirás artefactos, aparatos, lugares para personas o animales o plantas,
imagina cómo sería el lugar donde vives si pudieras hacer todo lo que has imaginado.

Ahora te invito a usar tu creatividad para contarnos lo siguiente:

* ¿Cómo sería ese lugar?
* ¿Qué harían las personas que están allí?
* ¿Qué necesitarían aprender?
* ¿Que recursos requieren?

Acompañado de lo anterior describe en tu cuaderno ese escenario que haz imaginado.

## Reflexiones

Dale play al siguiente audio para conocer el cierre de esta semana :)

<audio controls>
    <source src="./audio2-cierre-semana.mp3" type="audio/mpeg">
    Your browser does not support the audio element.
</audio>

> **Atención**: Sino puedes reproducir el audio, búscalo en la carpeta de las sesión 1 como:
>
> `audio2-cierre-semana.mp3`
