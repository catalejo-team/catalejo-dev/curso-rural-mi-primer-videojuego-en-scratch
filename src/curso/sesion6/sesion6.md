# Sesión 6: Hasta ahora empieza el viaje 

En este espacio queremos compartir nuestro trabajo realizado durante estas 6 semanas y reflexionar
sobre nuestros aprendizajes para ellos haz las siguientes actividades.

## Nuestro futuro

Mira este vídeo sobre un posible futuro y escribe una carta al yo del futuro (de 10 años en adelante)
donde le indiques o cuentes lo que sería para ti el futuro, pensando en el campo, las cosas
que quieres aprender o las cosas que quieres aprender, comparte tu carta en tu cuaderno.

<video style="max-width: 100%; height: auto;" controls >
    <source src="./video1-futuro.mp4" type="video/mp4">
</video> 

Vídeo tomado del siguiente [link de youtube](https://www.youtube.com/watch?v=ZHAaoEZGRZk)

> **Atención**: Sino puedes reproducir el vídeo, búscalo en la carpeta de las **sesión 6** como:
>
> `video1-futuro.mp4`


## Actividad 2: Comparte tu experiencia de aprendizaje

Hemos llegado al final de estas sesiones, quiero que te tomes un tiempo y pienses en todo
lo que hemos vivido y compartido, con los recursos que creaste para el vídeo juego
construye una historieta (guíate del siguiente vídeo que te dará una guía de como hacerla).
Finalmente graba un vídeo **donde cuentes y muestres la historieta que has creado**

Este vídeo contempla consejos para crear tu propia historieta, con cuaderno, papel y mucha imaginación.

<video style="max-width: 100%; height: auto;" controls >
    <source src="./video2-como-hacer-historieta.mp4" type="video/mp4">
</video> 

Vídeo tomado del siguiente [link de youtube](https://www.youtube.com/watch?v=C8K-eDx2boQ)

> **Atención**: Sino puedes reproducir el vídeo, búscalo en la carpeta de las **sesión 6** como:
>
> `video2-como-hacer-historieta.mp4`

En esta actividad final construirás tu propia historieta relacionando cada una de las sesiones de este curso.

## Actividad 3 (Opcional): Implementación de vídeo juego en scratch

Ve a los siguientes enlaces y realiza las tareas allí establecidas, empezarás a programar
tu primer vídeo juego:

- [Guardar proyecto en el computador](./juego-scratch/8.-Guardar-proyecto-en-el-computador/tutorial8.md)
- [Agregar indicador de teclas a usar](./juego-scratch/9.-Indicar-al-jugador-las-teclas-necesarias/tutorial9.md)
- [Demostración](./juego-scratch/0.-Archivo-videojuego/tutorial0.md)


<audio controls>
    <source src="./cierre.mp3" type="audio/mpeg">
    Your browser does not support the audio element.
</audio>

> **Atención**: Sino puedes reproducir el audio, búscalo en la carpeta de la **sesión 6** como:
>
> `cierre.mp3`

> Espero te haya servido para aprender nuevas cosas y ver nuevas oportunidades para tu desarrollo personal,
>
> Cordialmente,
>
> Johnny Cubides
