# Sesión 4: Semana de artefactos

## Actividad 1: Artefactos que permiten sentir y expresar

### Theremin el instrumento musical

El theremin es el único instrumento musical que se toca sin ser tocado, a continuación
te presentamos dos versiones de este instrumento musical.

Te invitamos a ver los vídeos de ambos theremin y reconocer lo que hace
el interprete en cada caso, responde las siguientes preguntas:

* ¿Qué se usa para tocar las notas musicales?
* ¿Que debe haber en el techo donde llegan la luz de los láser?
* ¿Tiene que ver en algo la distancia entre las manos y el theremin? Es decir, ¿la distancia
importa en algo?
* Piensa que todo está interconectado a una caja que genera sonido ¿Qué debería ir allí
dentro para que funcione de esa manera?
* Será que estos dispositivos requieren algún controlador que evalúe estados de
sensores y tome decisiones para generar sonido? Justifica tu respuesta

#### Theremin

<video style="max-width: 100%; height: auto;" controls >
    <source src="./theremin.mp4" type="video/mp4">
</video> 

> **Atención**: Sino puedes reproducir el vídeo, búscalo en la carpeta de las **sesión 4** como:
>
> `theremin.mp4`

#### Theremin láser

<video style="max-width: 100%; height: auto;" controls >
    <source src="./theremin-laser.mp4" type="video/mp4">
</video> 

> **Atención**: Sino puedes reproducir el vídeo, búscalo en la carpeta de las **sesión 4** como:
>
> `theremin-laser.mp4`

### Máquina musical de canicas de mármol

Te presentamos otro artefacto que permite crear música, te invitamos en este espacio
a observar los mecanismos, describir si hay un dispositivo en él que tome
decisiones, describe en tu cuaderno como funciona cada parte del instrumento musical.

<video style="max-width: 100%; height: auto;" controls >
    <source src="./maquina-marmol.mp4" type="video/mp4">
</video> 

> **Atención**: Sino puedes reproducir el vídeo, búscalo en la carpeta de las **sesión 4** como:
>
> `maquina-marmol.mp4`

### Actividad 2: Creando una mano robótica

En esta oportunidad queremos crear un artefacto denominado "mano robótica",
Te invitamos a construir tu propia mano robótica con materiales fáciles de
conseguir, por favor compárte fotografías de tu mano robótica.

<video style="max-width: 100%; height: auto;" controls >
    <source src="./mano-robotica.mp4" type="video/mp4">
</video> 

> **Atención**: Sino puedes reproducir el vídeo, búscalo en la carpeta de las **sesión 4** como:
>
> `mano-robotica.mp4`


## Actividad 3: Bocetos del vídeo juego

En esta actividad, te invitamos a que realices los bocetos de cada uno de los personajes
o elementos que quieres representar en tu vídeo juego.

Te planteamos las siguientes opciones:
- Personaje principal
- Enemigo(s)
- Elementos
- Entorno del video juego
- Momentos claves del video juego

El siguiente es un material de apoyo que te dará indicios de como construir personajes
a través de técnicas de dibujo, No olvides compartirlas en tu cuaderno.

<!-- insert in the document body -->
<object class="pdfobject-container" 
data='./curso-completo-de-dibujo-para-comics-christopher-hart.pdf#comment=Ejemplo&view=FitB&scrollbar=1&toolbar=1&statusbar=1&navpanes=1'
        type='application/pdf' 
        width='100%' 
        height='100%'>
<p>This browser does not support inline PDFs. Please download the PDF to view it: <a href="./curso-completo-de-dibujo-para-comics-christopher-hart.pdf">Download PDF</a></p>
</object>

## Actividad 4 (Opcional): Implementación de vídeo juego en scratch

Ve a los siguientes enlaces y realiza las tareas allí establecidas, empezarás a programar
tu primer vídeo juego:

- [Estructura y diseño de niveles](../../juego-scratch/2.-Estructura-de-Scratch-y-diseno-de-niveles/tutorial2.md)
- [Agregar personaje y movimientos](../../juego-scratch/3.-Agregar-personaje-y-movimiento-lineal-del-personaje/tutorial3.md)
- [Agregar animación al caminar](../../juego-scratch/4.-Animacion-de-personaje-caminando/tutorial4.md)
