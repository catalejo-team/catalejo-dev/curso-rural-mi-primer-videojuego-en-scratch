# Summary

- [Curso de Ciencia, Tecnología y Sociedad](./curso/README.md)
    - [Introducción](./curso/intro/introduccion.md)
    - [Sesión 1](./curso/sesion1/sesion1.md)
    - [Sesión 2](./curso/sesion2/sesion2.md)
    - [Sesión 3](./curso/sesion3/sesion3.md)
    - [Sesión 4](./curso/sesion4/sesion4.md)
    - [Sesión 5](./curso/sesion5/sesion5.md)
    - [Sesión 6](./curso/sesion6/sesion6.md)
- [Instalación de aplicaciones](./apps/apps.md)
    - [Instalación de apps en windows](./apps/win32/win32.md)
- [Mi primer juego en scratch](./juego-scratch/README.md)
    - [Iníciate en Scratch](./juego-scratch/1.-Abrir-Scratch/tutorial1.md)
    - [Estructura y diseño de niveles](./juego-scratch/2.-Estructura-de-Scratch-y-diseno-de-niveles/tutorial2.md)
    - [Agregar personaje y movimientos](./juego-scratch/3.-Agregar-personaje-y-movimiento-lineal-del-personaje/tutorial3.md)
    - [Agregar animación al caminar](./juego-scratch/4.-Animacion-de-personaje-caminando/tutorial4.md)
    - [Gravedad y muerte de personaje](./juego-scratch/5.-Agregar-gravedad-y-muerte-al-personaje/tutorial5.md)
    - [Meta y cambio de nivel](./juego-scratch/6.-Meta-y-cambio-de-nivel/tutorial6.md)
    - [Agregar enemigos y daño](./juego-scratch/7.-Agregar-enemigos-y-dano/tutorial7.md)
    - [Guardar proyecto en el computador](./juego-scratch/8.-Guardar-proyecto-en-el-computador/tutorial8.md)
    - [Agregar indicador de teclas a usar](./juego-scratch/9.-Indicar-al-jugador-las-teclas-necesarias/tutorial9.md)
    - [Demostración](./juego-scratch/0.-Archivo-videojuego/tutorial0.md)
- [Glosario](./glosario/README.md)

----
[Autores y colaboradores](misc/colaboradores.md)
