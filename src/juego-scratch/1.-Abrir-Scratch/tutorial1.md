# Scartch

Scratch es una plataforma de programación que nos permitirá crerar vídeojuegos, esta aplicación
la podrás instalar en tu computador desde este link:

[Link de descarga de Scratch 3](../../apps/win32/scratch-3.24.0-setup.exe)


## Instala Scratch en tu computador Windows

Esta es la versión para que instales en un computador con Windows

<video style="max-width: 100%; height: auto;" controls>
    <source src="./tutorial_1_a.mp4" type="video/mp4">
</video> 


## Iníciate con Scratch Online

Si tienes conexión a internet puedes iniciarte por acá

<video style="max-width: 100%; height: auto;" controls>
    <source src="./Tutorial_1.mp4" type="video/mp4">
</video> 
