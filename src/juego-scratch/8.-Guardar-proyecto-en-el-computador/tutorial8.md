# Guardar proyecto en el computador

En este video se muestra el proceso para guardar su proyecto en el computador, si realizó dicho proyecto sin haber creado cuenta en Scratch.

<video style="max-width: 100%; height: auto;" controls >
    <source src="./Tutorial_8.mp4" type="video/mp4">
</video> 
