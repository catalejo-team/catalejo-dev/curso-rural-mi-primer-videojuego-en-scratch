# Agregar personaje y movimiento lineal del personaje

En este video se agrega cualquier personaje, en este caso ROX unmonstruo amigable rojo y se le aplica movimiento lineal para que podamos controlar el personaje con las teclas derecha e izquierda.

<video style="max-width: 100%; height: auto;" controls>
    <source src="./Tutorial_3.mp4" type="video/mp4">
</video> 
