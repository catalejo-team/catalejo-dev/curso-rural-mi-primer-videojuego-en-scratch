# Estructura de Scratch y diseño de niveles

En este video se da a conocer con una pequeña explica la interfaz de Scratch, se indica en donde se pueden obtener recursos gratuitos y se diseñan los niveles para el juego.

<video style="max-width: 100%; height: auto;" controls>
    <source src="./Tutorial_2.mp4" type="video/mp4">
</video>

> En el caso de que no tengas acceso a internet. Puedes obtenerla en este link de
>
> [Recursos gratuitos](./recursos.zip).
