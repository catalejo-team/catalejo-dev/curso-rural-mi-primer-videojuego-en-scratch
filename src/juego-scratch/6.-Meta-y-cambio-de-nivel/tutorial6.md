# Meta y cambio de nivel

En este video se agrega un botón que representa la meta de cada nivel y el cambio del primer al segundo nivel cuando gane ROX, se agrega un audio de fondo además de un audio para que nos indique que hemos ganado.

<video style="max-width: 100%; height: auto;" controls >
    <source src="./Tutorial_6.mp4" type="video/mp4">
</video> 
