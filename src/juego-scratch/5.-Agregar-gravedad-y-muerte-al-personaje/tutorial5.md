# Gravedad y muerte de personaje

En este video se agrega la gravedad al personaje y la configuración 
cuando se pierda, por medio de resetear el juego y un audio que se reproduzca 
representando que hemos perdido.

<video style="max-width: 100%; height: auto;" controls >
    <source src="./Tutorial_5.mp4" type="video/mp4">
</video> 
