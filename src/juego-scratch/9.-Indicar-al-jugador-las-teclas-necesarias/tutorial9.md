# Agregar indicador de teclas a usar

En este video se agrega un objeto que representa las felchas del teclado, esto con el fin de mostrarlas para que el jugador sepa como moverse en el juego.

<video style="max-width: 100%; height: auto;" controls >
    <source src="./Tutorial_9.mp4" type="video/mp4">
</video> 
