# Animación de personaje caminando

En este video se realiza el movimiento pertinente que representa al personaje caminando.

<video style="max-width: 100%; height: auto;" controls >
    <source src="./Tutorial_4.mp4" type="video/mp4">
</video> 
