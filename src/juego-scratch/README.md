# Mi primer videojuergo con scratch

Tutorial 1: Abrir Scratch.

Descripción: En este video se realiza el proceso para abrir la aplicación
Scratch sin realizar el registro y con el proceso del registro. Dando las 
ventajas que se pueden obtener al registrarse.

Tutorial 2: Estructura de Scratch y diseño de niveles.

Descripción: En este video se explica brevemente la interfaz de Scratch, 
se indica en donde se pueden obtener recursos gratuitos y se diseñan los 
niveles para el juego.

Tutorial 3: Agregar personaje y movimiento lineal del personaje.

Descripción: En este video se agrega cualquier personaje, en este caso ROX un
monstruo amigable rojo y se le aplica movimiento lineal para que podamos controlar 
el personaje con las teclas derecha e izquierda.

Tutorial 4: Animación de personaje caminando.

Descipción: En este video se realiza el movimiento pertinente que representa 
al personaje caminando.

Tutorial 5: Agregar gravedad y muerte al personaje.

Descripción: En este video se agrega la gravedad al personaje y la configuración 
cuando se pierda, por medio de resetear el juego y un audio que se reproduzca 
representando que hemos perdido.

Tutorial 6: Meta y cambio de nivel.

Descripción: En este video se agrega un botón que representa la meta de cada
nivel y el cambio del primer al segundo nivel cuando gane ROX, se agrega un 
audio de fondo además de un audio para que nos indique que hemos ganado.

Tutorial 7: Agregar enemigos y daño.

Descripción: En este video se agregan 2 personajes como enemigos, los cuales
impediran que lleguemos a la meta, al perder se reproduce un sonido que nos 
indica dicha muerte y el juego se resetea al mismo nivel. Al igual se reubican
los objetos al cambiar de nivel.

Tutorial 8: Guardar proyecto en el computador.

Descripción: En este video se muestra el proceso para guardar su proyecto en el
computador, si realizó dicho proyecto sin haber creado cuenta en Scratch.

Tutorial 9: Indicar al jugador las teclas necesarias.

Descripción: En este video se agrega un objeto que representa las felchas del
teclado, esto con el fin de mostrarlas para que el jugador sepa como moverse en el 
juego.
